class BurgerMenu {
    toggle(menu) {
        const burgerLines = document.querySelectorAll(".burger-menu__line");
        for (const burgerLine of burgerLines) {
            burgerLine.classList.toggle("burger-menu__line--active");
        }
        menu.classList.toggle("nav-list--active");
    }
}