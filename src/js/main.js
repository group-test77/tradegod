"use strict"
const menu = document.querySelector(".nav-list");
document.addEventListener("click", (e) => {
    if  (e.target.closest("a")){
        e.preventDefault();
    }
    if (e.target.closest(".burger-menu") || !e.target.closest(".nav-list") && menu.className.includes("active")) {
        new BurgerMenu().toggle(menu);
    }
});
window.addEventListener("load", () => {
    if (window.innerWidth < 596) {
        new WordTransfer().addTransfer();
    }
});
window.addEventListener("orientationchange", () => {
    if (window.innerWidth < 596) {
        new WordTransfer().removeTransfer();
    } else {
        new WordTransfer().addTransfer();
    }
});


