const scrollElementsToTop = document.querySelectorAll('.scroll-animation-to-top');
const scrollElementsToRight = document.querySelectorAll('.scroll-animation-to-right');
function onEntry(entry) {
    entry.forEach(change => {
        if (change.isIntersecting) {
            if (change.target.className.includes("to-top")){
                change.target.classList.add('scroll-animation-to-top--show');
            }
            else {
                change.target.classList.add('scroll-animation-to-right--show');
            }
        }
    });
}
const observerToTop = new IntersectionObserver(onEntry, { threshold: [0] });
const observerToRight = new IntersectionObserver(onEntry, { threshold: [1,0] });
for (let elm of scrollElementsToTop) {
    observerToTop.observe(elm);
}
for (let elm of scrollElementsToRight) {
    observerToRight.observe(elm);
}