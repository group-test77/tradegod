class WordTransfer {
    title = document.querySelector(".content-list__title");
    addTransfer() {
        if (this.title.innerHTML.includes("<br>")){
            return;
        }
         const index = this.title.innerHTML.indexOf("для") - 1;
        this.title.innerHTML = `${this.title.innerHTML.slice(0, index)}<br>${this.title.innerHTML.slice(-index + 14)}`;
    }
    removeTransfer(){
        if (!this.title.innerHTML.includes("<br>")){
            return;
        }
        const index = this.title.textContent.indexOf("сервердля");
        this.title.textContent = `${this.title.textContent.slice(0, index+6)} ${this.title.textContent.slice(-13)}`;
    }
}