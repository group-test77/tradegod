import svgSprive from "gulp-svg-sprite";

export const svgSprites = (done) => {
    app.plugins.if(
        app.isBuild, app.gulp.src(app.path.src.svgIcons, {sourcemaps: true}))
        .pipe(app.plugins.if(
            app.isBuild, app.gulp.dest(`${app.path.build.images}/icons`)))
    app.gulp.src(app.path.src.svgIcons, {sourcemaps: true})
        .pipe(app.plugins.plumber(app.plugins.notify.onError({
            title: "SVG",
            massage: "Error: <%= error.message %>",
        })))
        .pipe(svgSprive({
            svg: {
                xmlDeclaration: false,
            },
                mode: {
                    stack: {
                        sprite: "svg-sprite.html",
                    },
                },
            })
        )
        .pipe(app.gulp.dest(app.path.build.svgSprites));
    done();
}
