import {deleteSync} from "del";
export const clean = (done)=> {
    deleteSync(app.path.clean.buildFolder);
    deleteSync(app.path.clean.mainHtml);
    // deleteSync(`${app.path.build.svgSprites}/stack`);
    done();
}
